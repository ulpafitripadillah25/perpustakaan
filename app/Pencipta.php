<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pencipta extends Model
{
    protected $table = 'pencipta';
    protected $fillable = ['name'];

    /**
     * Method One To Many 
     */
    public function transaksi()
    {
    	return $this->hasMany(Transaksi::class);
    }
}
