<?php

use Illuminate\Database\Seeder;

class BukusTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    \App\Buku::insert([
      [
        'id'        => 1,
        'judul'        => 'Belajar Pemrograman Java',
        'isbn'      => '9920392749',
        'pencipta_id'     => 1,
        'penerbit'    => 'PT. Restu Ibu',
        'tahun_terbit'  => 2018,
        'jumlah_buku'    => 20,
        'deskripsi'    => 'Buku Pertama Belajar Pemrograman Java Utk Pemula',
        'lokasi'      => 'rak1',
        'cover'      => 'buku_java.jpg',
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ],
      [
        'id'        => 2,
        'judul'        => 'Pemrograman Android',
        'isbn'      => '9920395559',
        'pencipta_id'     => 2,
        'penerbit'    => 'PT. Restu Guru',
        'tahun_terbit'  => 2018,
        'jumlah_buku'    => 14,
        'deskripsi'    => 'Jurus Rahasia Menguasai Pemrograman Android',
        'lokasi'      => 'rak2',
        'cover'      => 'jurus_rahasia.jpg',
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ],
      [
        'id'        => 3,
        'judul'        => 'Android Application',
        'isbn'      => '9920392000',
        'pencipta_id'     => 3,
        'penerbit'    => 'PT. Restu Ayah',
        'tahun_terbit'  => 2018,
        'jumlah_buku'    => 5,
        'deskripsi'    => 'Buku Pertama Belajar Pemrograman Java Utk Pemula',
        'lokasi'      => 'rak2',
        'cover'      => 'create_your.jpg',
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ],
      [
        'id'        => 4,
        'judul'        => 'Dasar-dasar Pemprograman Web',
        'isbn'      => '9786025728211',
        'pencipta_id'     => 4,
        'penerbit'    => ' FMIPA Universitas Negeri Semarang',
        'tahun_terbit'  => 2018,
        'jumlah_buku'    => 3,
        'deskripsi'    => 'Buku Dasar Pemrograman',
        'lokasi'      => 'rak2',
        'cover'      => 'pemrograman_web.jpg',
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ],
      [
        'id'        => 5,
        'judul'        => 'Algoritma dan pemprograman',
        'isbn'      => '9786024470753',
        'pencipta_id'     => 5,
        'penerbit'    => 'Noer Fikri Offset',
        'tahun_terbit'  => 2017,
        'jumlah_buku'    => 3,
        'deskripsi'    => 'Buku Algoritma pemrograman',
        'lokasi'      => 'rak2',
        'cover'      => 'algoritma.jpg',
        'created_at'      => \Carbon\Carbon::now(),
        'updated_at'      => \Carbon\Carbon::now()
      ],
    ]);
  }
}
